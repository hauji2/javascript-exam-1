import { firstGrownUp, firstOrange, firstLengthOver5Name } from "../src/find";

describe("array find test", () => {
  // Please add test cases here
  test("firstGrownUp", () => {
    const ages = [10, 20, 30, 40, 50];
    const result = firstGrownUp(ages);
    expect(result).toBe(20);
  });

  test("firstOrange", () => {
    const fruit = ["apple", "banana", "orange", "grape"];
    const result = firstOrange(fruit);
    expect(result).toBe("orange");
  });

  test("firstLengthOver5Name", () => {
    const names = ["Alice", "Bob", "Charlie", "David"];
    const result = firstLengthOver5Name(names);
    expect(result).toBe("Charlie");
  })
  })

