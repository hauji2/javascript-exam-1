import { addSerialNumber, halfNumbers, spliceNames } from "../src/map";

describe("array map test", () => {
  // Please add test cases here
  test("halfNumbers", () => {
    const numbers = [1, 2, 3, 4, 5];
    const result = halfNumbers(numbers);
    expect(result).toEqual([0.5, 1, 1.5, 2, 2.5]);
  });

  test("spliceNames", () => {
    const names = ["Alice", "Bob"];
    const result = spliceNames(names);
    expect(result).toEqual(["Hello Alice", "Hello Bob"]);
  });

  test("addSerialNumber", () => {
    const fruit = ["apple", "banana"];
    const result = addSerialNumber(fruit);
    expect(result).toEqual(["1. apple", "2. banana"]);
  })
});
