import { filterEvenNumbers, filterLengthWith4, filterStartWithA } from '../src/filter';

describe('array filter test', () => {
    // Please add test cases here
    test('test filterEvenNumbers', () => {
        const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        const result = filterEvenNumbers(numbers);
        expect(result).toEqual([2, 4, 6, 8, 10]);
    });

    test('test filterLengthWith4', () => {
        const words = ['apple', 'date'];
        const result = filterLengthWith4(words);
        expect(result).toEqual(['date']);
    });

    test('test filterStartWithA', () => {
        const letters = ['a', 'b', 'c', 'd', 'e', 'f'];
        const result = filterStartWithA(letters);
        expect(result).toEqual(['a']);
    })
    })
